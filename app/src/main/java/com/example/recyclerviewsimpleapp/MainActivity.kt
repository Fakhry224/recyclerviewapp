package com.example.recyclerviewsimpleapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        title = "225150401111005"

        val dataList: MutableList<Item> = mutableListOf()

        val inpCode: EditText = findViewById(R.id.txtCode)
        val inpDesc: EditText = findViewById(R.id.txtDesc)

        val btnSubmit: Button = findViewById(R.id.btnSimpan)

        val recyclerView: RecyclerView = findViewById(R.id.recycler_view)
        val adapter = DataAdapter(dataList)
        recyclerView.adapter = adapter

        btnSubmit.setOnClickListener {
            val code = inpCode.text.toString()
            val desc = inpDesc.text.toString()

            if(code.isNotBlank() && desc.isNotBlank()) {
                val avatarImageResId = R.drawable.image
                dataList.add(Item(code, desc, avatarImageResId))

                adapter.notifyDataSetChanged()

                inpCode.text.clear()
                inpDesc.text.clear()
            } else {
                Toast.makeText(this, "Kode dan deskripsi tidak boleh kosong", Toast.LENGTH_SHORT).show()
            }
        }
    }
}

data class Item(val code: String, val desc: String, val imageResId: Int)

class DataAdapter(private val dataList: MutableList<Item>) :
        RecyclerView.Adapter<DataAdapter.DataViewHolder>() {

            class DataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
                private val avatar: ImageView = itemView.findViewById(R.id.imageView)
                private val codeTextView: TextView = itemView.findViewById(R.id.txtCodeItem)
                private val descTextView: TextView = itemView.findViewById(R.id.txtDescItem)

                fun bind(item: Item){
                    avatar.setImageResource(item.imageResId)
                    codeTextView.text = item.code
                    descTextView.text = item.desc
                }
            }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.data_item, parent, false)
        return DataViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        holder.bind(dataList[position])
    }

    override fun getItemCount(): Int {
        return dataList.size
    }
}
